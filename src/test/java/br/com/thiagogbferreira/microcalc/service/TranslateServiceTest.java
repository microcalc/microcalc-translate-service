package br.com.thiagogbferreira.microcalc.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(MockitoJUnitRunner.class)
public class TranslateServiceTest {

  TranslateService service = new TranslateService();
  TranslateRepository repository;
  TranslateServiceApplication app = new TranslateServiceApplication();
  
  @Before
  public void setup() {
    repository = Mockito.mock(TranslateRepository.class);
    ReflectionTestUtils.setField(service, "repository", repository);
    ReflectionTestUtils.setField(app, "service", service);

  }

  @Test
  public void consultOnRepositoryServiceTranslated() {
    Mockito.when(repository.findOne("+")).thenReturn( new TranslateDomain("+", "sum"));
    ResponseEntity<String> ret = app.translateService("+");
    assertThat(ret.getStatusCodeValue()).isEqualTo(200);
    assertThat(ret.getBody()).isEqualTo("sum");
  }

  @Test
  public void consultOnRepositoryServiceTranslatedAndReturnTheInputOnNotFound() {
    Mockito.when(repository.findOne("sum")).thenReturn( null );
    ResponseEntity<String> ret = app.translateService("sum");
    assertThat(ret.getStatusCodeValue()).isEqualTo(200);
    assertThat(ret.getBody()).isEqualTo("sum");
    verify(repository, times(1)). findOne("sum");
  }

  @Test
  public void saveOnRepositoryTranslationSended() {
    Map<String, String> map = new HashMap<>();
    map.put("key", "+");
    map.put("operation", "sum");
    TranslateDomain domain = new TranslateDomain("+", "sum");
    Mockito.when(repository.save( domain )).thenReturn( domain);
    ResponseEntity<String> ret = app.create( map );
    
    assertThat(ret.getStatusCodeValue()).isEqualTo(200);
    assertThat(ret.getBody()).isEqualTo("Success");
    
    verify(repository, times(1)). save(domain);
  }

}
