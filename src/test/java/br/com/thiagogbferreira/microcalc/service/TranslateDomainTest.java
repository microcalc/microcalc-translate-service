package br.com.thiagogbferreira.microcalc.service;

import org.junit.Test;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

public class TranslateDomainTest {
  @Test
  public void equalsContract() {
    
      EqualsVerifier
        .forClass(TranslateDomain.class)
        .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
        .withOnlyTheseFields("key")
        .verify();
  }
  
}
