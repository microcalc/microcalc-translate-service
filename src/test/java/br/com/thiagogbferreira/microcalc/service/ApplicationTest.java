package br.com.thiagogbferreira.microcalc.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.springframework.boot.test.rule.OutputCapture;

public class ApplicationTest {
  @Rule
  public OutputCapture capture = new OutputCapture();

  @Test
  public void validateIfSpringBootApplicationIsStartingCallingTheMainMethod() {
    System.setProperty("server.port", "0");
    TranslateServiceApplication.main(new String[] {});
    assertThat(capture.toString()).contains("Started TranslateServiceApplication in");
  }
}
