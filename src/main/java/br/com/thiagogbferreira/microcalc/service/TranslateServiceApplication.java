package br.com.thiagogbferreira.microcalc.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Thiago Ferreira
 *
 */
@SpringBootApplication
@RestController
public class TranslateServiceApplication {
  
  @Autowired
  private TranslateService service;
  
  /**
   * @param args
   */
  public static void main(String[] args) {
    SpringApplication.run(TranslateServiceApplication.class, args);
  }
  
  /**
   * @param param
   * @return
   */
  @RequestMapping("/{param:.+}")
  public ResponseEntity<String> translateService(@PathVariable("param") String param) {
    return ResponseEntity.ok(service.getOperation(param));
  }
  
  /**
   * @param param
   * @return
   */
  @PostMapping("/")
  public ResponseEntity<String> create(@RequestBody Map<String,String> map) {
    service.save(map.get("key"), map.get("operation"));
    return ResponseEntity.ok("Success");
  }


  
}
