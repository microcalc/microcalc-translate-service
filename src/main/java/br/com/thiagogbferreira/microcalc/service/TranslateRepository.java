package br.com.thiagogbferreira.microcalc.service;

import org.springframework.data.mongodb.repository.MongoRepository;


public interface TranslateRepository extends MongoRepository<TranslateDomain, String> {

}

