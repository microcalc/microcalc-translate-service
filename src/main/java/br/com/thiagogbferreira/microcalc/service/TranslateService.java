package br.com.thiagogbferreira.microcalc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
//@EnableCaching
public class TranslateService {

  @Autowired
  private TranslateRepository repository;

  //@Cacheable("operations")
  public String getOperation(String key) {
    TranslateDomain kv = repository.findOne(key);
    return kv==null?key:kv.getValue();
  }

  //@CachePut(key="#key", value="operations")
  public String save(String key, String operation){
    repository.save(new TranslateDomain(key, operation));
    return operation;
  }
}

