package br.com.thiagogbferreira.microcalc.service;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@AllArgsConstructor
@EqualsAndHashCode(of = { "key" })
@Document(collection = "translations")
public final class TranslateDomain {

  @Id
  private final String key;

  @Getter
  private final String value;

}
